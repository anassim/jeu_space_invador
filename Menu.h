#pragma once
#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <windows.h>
#include <mmsystem.h>
#include "UIKit.h"
#include "Coord.h"
#include "ExtraTerrestre.h"
#include "Laser.h"
#include "Martien.h"
#include "Vaisseau.h"
#include "Tire.h"
#include "Alien.h"
#include "Player.h"
#include "Menu.h"
class Menu {
	int niveau;
	int vie;
	int nbAlien;

public:
	Menu();
	void partie();
	void afficherMenu();
	void choix();
	int getNiveau();
	static char recupererTouche();
	static void validationSaisie(char &rejouer);
};