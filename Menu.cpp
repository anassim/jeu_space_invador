#include "Menu.h"
#include <iostream>
#include <conio.h>
#pragma comment(lib, "Winmm.lib")

Menu::Menu() {
}

void Menu::partie()
{
	//des donnees pour la fenetre
	UIKit::setDimensionFenetre(0, 0, 143, 44);
	UIKit::cadre(0, 0, 102, 42, 11);
	UIKit::cadre(103, 0, 142, 42, 10);
	UIKit::color(13);
	UIKit::gotoXY(110, 20); std::cout << "Droite -> : L";
	UIKit::gotoXY(110, 22); std::cout << "Gauche <- : K";
	UIKit::gotoXY(110, 24); std::cout << "Tire : A";
	UIKit::curseurVisible(false);
	srand(time(0));

	//creer le vaisseau les lasers et martiens les initialiser
	Player vaisseau;
	int nbLaser = 0;
	Tire* laser = new Tire[nbLaser];

	//reation des martiens
	Alien** martien = new Alien*[infoAlien::nbAlien];

	for (int i(0); i < this->nbAlien; i++)
	{
		martien[i] = Alien::creerAlien();
		martien[i]->resetExtraTerrestre();
		martien[i]->putExtraTerrestre();
	}
	//variable pour la touche appuye
	char key;
	int fin = 1;

	//----------------------------------------------
	do {
		//recuperer la touche si elle appuye
		key = recupererTouche();

		//modifier la position du vaisseau si k ou l est appuye et verifier si on touche un laser
		vaisseau.modifierPosition(key);
		vaisseau.laserTouche();

		//si a est appuye donc lancer un laser
		if (toupper(key) == 'A')
		{
			PlaySound("shoot.wav", NULL, SND_FILENAME | SND_ASYNC);
			Tire::ajouterLaser(laser, nbLaser);
			laser[nbLaser - 1].startLaser(vaisseau.coord.getPositionX(), 38, defaultLaser::laserParDefault);
		}
		else if(toupper(key) == 'P')
		{
			while(!_kbhit()){}
		}

		//deplacer les lasers du vaisseau
		for (int i(0); i < nbLaser; i++)
		{
			laser[i].moveLaser(1);//il faut heriter la fonction en ajoutant des parametres comme les martiens
			//verifier si le laser touche un Martien

			if (!laser[i].isAlive && laser[i].coord.getPositionY()) //on change la condition apres lheritage
			{
				for (int j(0); j < ExtraTerrestre::getNombreExtraTerrestre(); j++)
				{
					if ((*martien[j]).coord.getPositionX() == laser[i].coord.getPositionX() && (*martien[j]).coord.getPositionY() == laser[i].coord.getPositionY())
					{
						vaisseau.point += martien[j]->ajouterPoints();
						(*martien[j]).removeExtraTerrestre();
						Alien::supprimerMartien(martien, j);
						j = ExtraTerrestre::getNombreExtraTerrestre();
						PlaySound("explosion.wav", NULL, SND_FILENAME | SND_ASYNC);
					}
				}
			}

			//si un laser est mort on le supprime du tableau
			if (!laser[i].isAlive)
			{
				Tire::supprimerTire(laser, nbLaser, i);
				i--;
			}
		}

		//faire bouger les Martiens et les faire tirer et les faire jiggler et bouger
		for (int i(0); i < ExtraTerrestre::getNombreExtraTerrestre(); i++)
		{
			//pour le jiggle des martiens
			if (fin % (5 * this->niveau) == 0)
			{
				if (martien[i]->getTypeAlien() == 'C')
				{
					martien[i]->bougerAlien();
				}
				else
				{
					martien[i]->jiggleMartien();
				}
			}

			//pour le tire des martiens
			if (fin % (rand() % 20 + (this->niveau * 10)) == 0)
			{
				martien[i]->tireEnemie();
			}
		}

		//pour le deplacement des lasers Alien
		if (fin % (2 + this->niveau) == 0)
		{
			for (int j(0); j < Alien::getNbLaserAlien(); j++)
			{
				if (Alien::laserAlien[j].isAlive)
				{
					Alien::laserAlien[j].moveLaser(2);
				}
			}
		}
		vaisseau.laserTouche();//verifier si un laser touche le vaisseau

		//pour l'apparition des aliens dans le terrain
		if (fin % 100 == 0)
		{
			martien[Alien::getNombreExtraTerrestre()] = Alien::creerAlien();
			martien[Alien::getNombreExtraTerrestre()]->resetExtraTerrestre();
			martien[Alien::getNombreExtraTerrestre() - 1]->putExtraTerrestre();
		}

		//faire le ralentie
		Sleep(40);
		fin++;
		UIKit::color(13);
		UIKit::gotoXY(110, 2); std::cout << "Vie : "<< vaisseau.vie;
		UIKit::gotoXY(110, 4); std::cout << "Point : "<< vaisseau.point;
		UIKit::gotoXY(110, 6); std::cout << "nb ExtraTerrestres : " << Alien::getNombreExtraTerrestre();


	} while (fin < 15000 && ExtraTerrestre::getNombreExtraTerrestre() > 0 && ExtraTerrestre::getNombreExtraTerrestre() < infoAlien::nbAlien && vaisseau.vie > 0);


	//-----------------------------------------------------------------------

	/*pour la fin du jeu*/
	UIKit::gotoXY(25, 20);
	if (Martien::getNombreExtraTerrestre() == 0) {

		std::cout << "----------------- BRAVOOOOOO ------------------";
		PlaySound("good_game.wav", NULL, SND_FILENAME | SND_ASYNC);
	}
	else {
		std::cout << "----------------- GAME OVER -------------------";
		PlaySound("game_over.wav", NULL, SND_FILENAME | SND_ASYNC);
	}
	Sleep(3000);

	while (!_kbhit())
	{

	}
	

	//liberer la memoire pour s'assurer
	delete[] laser;
	for (int i(0); i < Alien::getNombreExtraTerrestre(); i++)
	{
		delete martien[i];
	}
	delete[] martien;
	Alien::resetNombreExtraTerrestre();
	Alien::resetNbLaserAlien();

}

void Menu::afficherMenu() {
	system("cls");

	UIKit::cadre(40, 9, 60, 20,11);
	UIKit::gotoXY(20, 2);
	UIKit::color(12);
	std::cout << R"(                          .__                         .___                   
	    _________________    ____  ____   |__| _______  _______     __| _/___________  ______ 
	   /  ___/\____ \__  \ _/ ___\/ __ \  |  |/    \  \/ /\__  \   / __ |/ __ \_  __ \/  ___/ 
	  \___ \ |  |_ > > __ \\  \__\  ___/  |  |   |  \   /  / __ \_/ /_ /\  ___/|  | \/\___ \  
	  /____  >|   __(____  /\___  >___  > |__|___|  /\_/  (____  /\____ |\___  >__| /____  >
	       \/ |__|       \/     \/    \/          \/           \/      \/    \/           \/ )" << std::endl << std::endl << std::endl;
	
	UIKit::color(10);
	UIKit::gotoXY(45, 10);
	std::cout << "Facile(1)" << std::endl;
	UIKit::gotoXY(45, 12);
	std::cout << "Moyen(2)" << std::endl;
	UIKit::gotoXY(45, 14);
	std::cout << "Difficile(3)" << std::endl;
	UIKit::gotoXY(45, 16);
	this->choix();
}

void Menu::choix() {
	char userOption;
	do {
		userOption = _getch();
		switch (userOption) {
		case '1': this->vie = 5; this->niveau = 3; this->nbAlien = 2; break;
		case '2': this->vie = 3; this->niveau = 2; this->nbAlien = 4; break;
		case '3': this->vie = 1; this->niveau = 1; this->nbAlien = 6; break;
		default:UIKit::gotoXY(43, 16); std::cout << "Choisir 1/2/3 : " << std::endl; UIKit::gotoXY(43, 17); break;
		}
	} while (userOption != '1' && userOption != '2' && userOption != '3');

	system("cls");
}

int Menu::getNiveau() {
	return this->niveau;
}

char Menu::recupererTouche()
{
	if (_kbhit())
	{
		return _getch();
	}

	return -1;
}

void Menu::validationSaisie(char &rejouer)
{
	/*while ((cin.fail() || cin.peek() == '/n') && (toupper(rejouer) != 'O' && toupper(rejouer) != 'N'))
	{
		cin.clear();
		cin.ignore(512, '/n');
		system("cls");
		UIKit::gotoXY(30, 15);
		cout << "voulez vous rejouer la partie ?? (o/n)" << endl;
		UIKit::gotoXY(30, 17);
		cout << "--->>>>> ";
		cin >> rejouer;
	}*/
	do
	{
		system("cls");
		UIKit::gotoXY(30, 15);
		std::cout << "voulez vous rejouer la partie ?? (o/n)" << endl;
		UIKit::gotoXY(30, 17);
		std::cout << "--->>>>> ";
		rejouer = _getch();
		std::cout << rejouer;
	}while(toupper(rejouer) != 'O' && toupper(rejouer) != 'N');
}