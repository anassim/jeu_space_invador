#pragma once
#include "Vaisseau.h"
#include "UIKit.h"

class Player : public Vaisseau
{
protected:
	void putVaisseau() const;
	void removeVaisseau() const;
public:
	int vie;
	int point;

	Player();
	virtual void modifierPosition(char);
	void laserTouche();
};