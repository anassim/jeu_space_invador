#include <stdlib.h>
#include "Martien.h"
#include "Alien.h"

int Alien::nbLaserAlien = 0;
Tire *Alien::laserAlien = new Tire[infoAlien::maxTire];

Alien::Alien(): Martien(0, 0) {}

Alien::Alien(int type, int valeur) : Martien(type, valeur)
{
	int typeL = rand() % 8;
	if (typeL == 0 || typeL == 1 || typeL == 2 || typeL == 3)
	{
		this->typeLaser = '|';
	}
	else if (typeL == 4 || typeL == 5)
	{
		this->typeLaser = 'o';
	}
	else if (typeL == 6)
	{
		this->typeLaser = '\\';
	}
	else if (typeL == 7)
	{
		this->typeLaser = '/';
	}
}

Alien* Alien::creerAlien()
{
	int typeMartien;
	int valeurMartien;

	typeMartien = rand() % 3;
	if (typeMartien == 0)
	{
		valeurMartien = 20;
	}
	else if (typeMartien == 1)
	{
		valeurMartien = 10;
	}
	else if (typeMartien == 2)
	{
		valeurMartien = 25;
	}

	Alien* alien = new Alien(typeMartien+65, valeurMartien);
	
	return alien;
}

void Alien::putExtraTerrestre() const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	UIKit::color(10);
	cout << char(typeExtraTerrestre);
}

void Alien::resetExtraTerrestre()
{
	if (this->typeLaser == '/')
	{
		coord.setPositionX(rand() % 50 + 50);
		do {
			coord.setPositionY(rand() % 30 + 1);
		} while (coord.getPositionY() > (0.4 * coord.getPositionX()));
	}
	else if (this->typeLaser == '\\')
	{
		coord.setPositionX(rand() % 50);
		do {
			coord.setPositionY(rand() % 30 + 1);
		} while (coord.getPositionY() < (0.4 * coord.getPositionX()));
	}
	else
	{
		coord.setPositionX(rand() % 100 + 2);
		coord.setPositionY(rand() % 30 + 1);
	}
	isAlive = true;
	nombreExtraTerrestre++;
}

int Alien::getNbLaserAlien()
{
	return Alien::nbLaserAlien;
}

void Alien::augmenterNbAlien()
{
	Alien::nombreExtraTerrestre++;
}

void Alien::resetNbLaserAlien()
{
	Alien::nbLaserAlien = 0;
}

void Alien::supprimerMartien(Alien** ptr_martiens, int &indexMartien)
{
	if (indexMartien != Alien::getNombreExtraTerrestre())
	{
		ptr_martiens[indexMartien] = ptr_martiens[Alien::getNombreExtraTerrestre() - 1];
	}
	Alien::reduireNombreExtraTerrestre();
}

int Alien::getTypeAlien()
{
	return this->typeExtraTerrestre;
}

void Alien::bougerAlien()
{
	this->removeExtraTerrestre();

	int direction;
	bool bouge = false;

	do {
		direction = rand() % 4;
		switch (direction)
		{
		case 0:
			if (coord.getPositionX() < 99 && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'A' && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'B' && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'C')
			{
				coord.setPositionX(coord.getPositionX() + 1);
				bouge = true;
			}
			break;
		case 1:
			if (coord.getPositionX() > 1 && UIKit::getCharXY(coord.getPositionX() - 1, coord.getPositionY()) != 'A' && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'B'&& UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'C')
			{
				coord.setPositionX(coord.getPositionX() - 1);
				bouge = true;
			}
			break;
		case 2:
			if (coord.getPositionY() > 1 && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'A' && UIKit::getCharXY(coord.getPositionX(), coord.getPositionY() - 1) != 'B'&& UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'C')
			{
				coord.setPositionY(coord.getPositionY() - 1);
				bouge = true;
			}
			break;
		case 3:
			if (coord.getPositionY() < 30 && UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'A' && UIKit::getCharXY(coord.getPositionX(), coord.getPositionY() + 1) != 'B'&& UIKit::getCharXY(coord.getPositionX() + 1, coord.getPositionY()) != 'C')
			{
				coord.setPositionY(coord.getPositionY() + 1);
				bouge = true;
			}
			break;
		}
	} while (bouge == false);

	this->putExtraTerrestre();
}

void Alien::tireEnemie()
{
	UIKit::color(12);
	if (Alien::nbLaserAlien < infoAlien::maxTire)
	{
		Alien::laserAlien[Alien::nbLaserAlien].startLaser(this->coord.getPositionX(),this->coord.getPositionY(), this->typeLaser);
		Alien::nbLaserAlien++;
	}
	else
	{
		Alien::nbLaserAlien = 0;
		Alien::laserAlien[Alien::nbLaserAlien].startLaser(coord.getPositionX(), this->coord.getPositionY(), this->typeLaser);
	}
}