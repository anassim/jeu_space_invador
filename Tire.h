#pragma once

#include "Coord.h"
#include "Laser.h"

enum defaultLaser {
	laserParDefault = '*',
};

class Tire : public Laser
{


public :
	char typeLaser;

	virtual void startLaser(int, int, char);
	virtual void putLaser(char &typeLaser) const;
	virtual void moveLaser(unsigned short direction);
	void changerPosition();
	bool laserMort();
	static void supprimerTire(Tire* &ptr_lasers, int &nbTire, int &indexLaser);
	static void ajouterLaser(Tire* &ptr_lasers, int &nbLaser);
};