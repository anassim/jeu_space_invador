#pragma once

#include "ExtraTerrestre.h"
#include "Martien.h"
#include "Tire.h"
#include "UIKit.h"

enum infoAlien {
	nbAlien = 15,
	maxTire = 40 * nbAlien
};

class Alien : public Martien
{
	static int nbLaserAlien;

public :
	static Tire *laserAlien;
	char typeLaser;

	Alien();
	Alien(int, int);
	int getTypeAlien();
	static Alien* creerAlien();
	void putExtraTerrestre() const;
	static int getNbLaserAlien();
	static void augmenterNbAlien();
	static void resetNbLaserAlien();
	void bougerAlien();
	void resetExtraTerrestre();
	static void supprimerMartien(Alien** ptr_martiens, int &indexMartien);
	void tireEnemie();
};