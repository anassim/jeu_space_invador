#include <iostream>
#include "Menu.h"

using namespace std;

int main()
{
	char rejouer;
	Menu* menu;
	//creer le menu et lancer la partie
	do {
		//creer le menu et commencer la partie
		menu = new Menu;
		menu->afficherMenu();
		menu->partie();

		//demander a l'utilisateur si il veut recommencer la partie
		Menu::validationSaisie(rejouer);
		
		delete menu;
		menu = NULL;

	} while (toupper(rejouer) != 'N');


	return 0;
}

//---------------------------------------------------------