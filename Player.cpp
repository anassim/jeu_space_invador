#include "Player.h"
#include "Tire.h"
#include "Alien.h"
#include <iostream>


void Player::putVaisseau() const 
{
	UIKit::color(9);
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << '+';
	coord.gotoXY(coord.getPositionX() + 1, coord.getPositionY() + 1);
	cout << '+';
	coord.gotoXY(coord.getPositionX() - 1, coord.getPositionY() + 1);
	cout << '+';
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << '+';
}

void Player::removeVaisseau() const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << " ";
	coord.gotoXY(coord.getPositionX() + 1, coord.getPositionY() + 1);
	cout << " ";
	coord.gotoXY(coord.getPositionX() - 1, coord.getPositionY() + 1);
	cout << " ";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << " ";
}

Player::Player() :Vaisseau()
{
	this->vie = 3;
	this->point = 0;
	coord.setPositionX(20);
	coord.setPositionY(39);
	this->putVaisseau();
}
void Player::modifierPosition(char key) {
	removeVaisseau();
	if (this->coord.getPositionX() > 2 && this->coord.getPositionX() < 100)
	{
		switch (toupper(key)){
		case 'K':coord.setPositionX(coord.getPositionX() - 1);break;
		case 'L':	coord.setPositionX(coord.getPositionX() + 1);break;
		}
	}
	else if (this->coord.getPositionX() == 100 && toupper(key) == 'K') {
		coord.setPositionX(coord.getPositionX() - 1);
	}
	else if (this->coord.getPositionX() == 2 && toupper(key) == 'L') {
		coord.setPositionX(coord.getPositionX() + 1);
	}
	putVaisseau();
}

void Player::laserTouche(){
	for (int i(0); i < Alien::getNbLaserAlien(); i++)
	{
		if (UIKit::getCharXY(Alien::laserAlien[i].coord.getPositionX(),Alien::laserAlien[i].coord.getPositionY()) == '+' && Alien::laserAlien[i].isAlive) {
			this->vie--;	
			Alien::laserAlien[i].isAlive = false;
		}
	}
}