#include "Laser.h"
#include "UIKit.h"
#include "Tire.h"
#include <iostream>
using namespace std;

void Tire::startLaser(int x, int y, char typeLaser)
{
	coord.setPositionX(x);
	coord.setPositionY(y + 1);
	this->typeLaser = typeLaser;
	putLaser(typeLaser);
	isAlive = true;
}
void Tire::putLaser(char &typeLaser) const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << typeLaser;
}
void Tire::moveLaser(unsigned short direction)
{
	removeLaser();
	if (direction == 1)
	{
		UIKit::color(9);
			coord.setPositionY(coord.getPositionY() - 1);
			if (!this->laserMort())
			{
				putLaser(this->typeLaser);
			}
			else
			{
				this->killLaser();
			} 
	}
	else if (direction == 2)
	{
		UIKit::color(12);
		changerPosition();
			if (!this->laserMort())
			{
				putLaser(this->typeLaser);
			}
			else
			{
				this->killLaser();
			}
	}
}

void Tire::changerPosition()
{
	if (this->typeLaser == '|')
	{
		coord.setPositionY(coord.getPositionY() + 1);
	}
	else if (this->typeLaser == 'o')
	{
		coord.setPositionY(coord.getPositionY() + 1);
	}
	if (this->typeLaser == '\\')
	{
		coord.setPositionY(coord.getPositionY() + 1);
		coord.setPositionX(coord.getPositionX() + 1);
	}
	if (this->typeLaser == '/')
	{
		coord.setPositionY(coord.getPositionY() + 1);
		coord.setPositionX(coord.getPositionX() - 1);
	}
}

bool Tire::laserMort()
{
	char ilYaQuoi = UIKit::getCharXY(coord.getPositionX(), coord.getPositionY());
	if (coord.getPositionX() == 1 || coord.getPositionX() == 99 || coord.getPositionY() == 41 || coord.getPositionY() == 1)
	{
		return true;
		
	}
	else if (ilYaQuoi == char(32) || ilYaQuoi == '|' || ilYaQuoi == '\\' || ilYaQuoi == '/' || ilYaQuoi == 'o' || ilYaQuoi == defaultLaser::laserParDefault || ilYaQuoi == '+')
	{
		return false;
	}
	/*else
	{
		return true;
	}*/
}

void Tire::supprimerTire(Tire* &ptr_Tires, int &nbTire, int &indexTire)
{
	Tire* nvTire = new Tire[nbTire - 1];

	for (int i(indexTire); i < nbTire - 1; i++)
	{
		ptr_Tires[i] = ptr_Tires[i + 1];
	}
	for (int i(0); i < nbTire - 1; i++)
	{
		nvTire[i] = ptr_Tires[i];
	}

	delete[] ptr_Tires;
	ptr_Tires = nvTire;
	nvTire = NULL;

	nbTire--;
}

void Tire::ajouterLaser(Tire* &ptr_lasers, int &nbLaser)
{
	//voir si le 1er laser est arrive y=0 pour le supprimer
	nbLaser++;

	Tire* nvLaser = new Tire[nbLaser];//creer un nv tableau selon le nb de laser vivant

	//copier les donnees des anciens lasers au nv lasers
	for (int i(0); i < nbLaser - 1; i++)
	{
		nvLaser[i] = ptr_lasers[i];
	}

	//recopier le tableau de nv laser dans l'ancien laser pour continuer a jouer et initialiser le nb laser a NULL
	delete[] ptr_lasers;
	ptr_lasers = nvLaser;

	nvLaser = NULL;
}